package com.redbonesolutions.highline.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

@Entity
@Table (name = "ride")
public class Ride extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 1L;	
	
	private String date_created;
	private String last_updated;
	private int active;
	@Column(name="[name]")
	private String name;
	private String description;
	private int user_id;
	private int group_id;
	private int location_id;
	private int address_id;
	private String date;
	private String time;
	private String status;
	private int joinable;
	private String tempo;
	@Column(name="[drop]")
	private int drop;
	private int available;
	
	public Ride() {
		
	}
	
	// riders
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="ride_members", 
		joinColumns=@JoinColumn(name="ride_id", referencedColumnName="id"),
		inverseJoinColumns=@JoinColumn(name="user_id", referencedColumnName="id"))
	private Set<User> riders;
	
	public String getDate_created() {
		return date_created;
	}

	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}

	public String getLast_updated() {
		return last_updated;
	}

	public void setLast_updated(String last_updated) {
		this.last_updated = last_updated;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getGroup_id() {
		return group_id;
	}

	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}

	public int getAddress_id() {
		return address_id;
	}

	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getJoinable() {
		return joinable;
	}

	public void setJoinable(int joinable) {
		this.joinable = joinable;
	}

	public String getTempo() {
		return tempo;
	}

	public void setTempo(String tempo) {
		this.tempo = tempo;
	}

	public int getDrop() {
		return drop;
	}

	public void setDrop(int drop) {
		this.drop = drop;
	}

	public int getAvailable() {
		return available;
	}

	public void setAvailable(int available) {
		this.available = available;
	}
			
	public Set<User> getRiders() {
		return riders;
	}

	public void setRiders(Set<User> riders) {
		this.riders = riders;
	}

	@Override
	@JsonIgnore
	public boolean isNew() {
		return false;
	}

	@Override
	public String toString() {
		return "Ride [date_created=" + date_created + ", last_updated="
				+ last_updated + ", active=" + active + ", name=" + name
				+ ", description=" + description + ", user_id=" + user_id
				+ ", group_id=" + group_id + ", location_id=" + location_id
				+ ", address_id=" + address_id + ", date=" + date + ", time="
				+ time + ", status=" + status + ", joinable=" + joinable
				+ ", tempo=" + tempo + ", drop=" + drop + ", available="
				+ available + ", riders=" + riders + "]";
	}	
		
}
