'use strict';
 
highlineControllers.controller('SandboxController', ['$scope', '$timeout', function($scope, $timeout) {

	$scope.test = 'Test Sandbox';
	$scope.dead = {
		leadGuitar: 'Garcia',
		rhythmGuitar: 'Weir',
		bass: 'Lesh',
		keyboards: 'Godcheaux'
	};
	
	$scope.format = 'yyyy-MM-dd HH:mm:ss a';
	
	$scope.name = 'Frankenstein';
	$scope.message = '';
		
	$scope.hideDialog = function(message) {
		$scope.message = message;
		$scope.dialogIsHidden = true;
		$timeout(function() {
			$scope.message = '';
			$scope.dialogIsHidden = false;
		}, 2000);
	};
	
	$scope.display = "sucka";
	
	$scope.showText = function(text) {
		$scope.display = angular.toJson(text, true);
	}
	
}]);
