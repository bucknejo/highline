'use strict';

highlineControllers.controller('RegisterController', ['$scope', '$http', '$log', '$location', function($scope, $http, $log, $location) {
	
	$scope.test = '';
	
	$scope.user = {
		email: '',
		password: ''
	};	
		
	$scope.request = {
		method: 'POST',
		url: '/highline/service/register',
		data: $scope.user
	};
	
	// register
	$scope.register = function(form) {
						
		$log.info('register user: ' + JSON.stringify($scope.request));
		
		$http($scope.request).then(function success(response) {
			$log.info('success: ' + JSON.stringify(response));
			$location.path('/users/detail/' + response.data.id);
		}, function error(response){
			$log.info('error: ' + JSON.stringify(response));
		});		
		
	};	

	// cancel
	$scope.cancel = function() {
		$log.info('test: ' + JSON.stringify($scope.user));
	};
	
}]);

