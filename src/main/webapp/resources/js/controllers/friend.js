'use strict';

highlineControllers.controller('FriendController', ['$scope', '$routeParams', 'Friend', 'Member', '$log', function($scope, $routeParams, Friend, Member, $log) {

	$scope.invite = function(id) {
		// TODO implement invite business logic
	};
			
	$scope.isInvite = function(status) {		
		if (status === 0) return true;
	};
	
	$scope.remove = function(id) {
		// TODO implement removal business logic
	};
	
	$scope.isRemove = function(status) {
		if (status === 1) return true;
	}	
	
	$scope.friendStatus = function(status) {		
		var cls = ['blue', 'green', 'yellow', 'red'];
		return cls[status];
	};
	
	$scope.friends = Friend.query({id: $routeParams.id}, function() {
		$log.info('Friends: ' + JSON.stringify($scope.friends));
	});
	
	$scope.members = Member.query({id: $routeParams.id}, function() {
		$log.info('Members: ' + JSON.stringify($scope.members));
	});

}]);
