/**
 * 
 */

'use strict';
 
var highlineDirectives = angular.module('highlineDirectives', []);

highlineDirectives.directive('sandbox', function(){
	return {
		restrict: 'E',
		scope: {
			bandInfo: '=band'
		},
		templateUrl: '/highline/resources/js/templates/directive-template-sandbox.html'
	}
});

highlineDirectives.directive('currentTime', ['$interval', 'dateFilter', function($interval, dateFilter){

	function link(scope, element, attrs) {
		var format, timeoutId;
		
		function updateTime(){
			element.text(dateFilter(new Date(), format));
		}
		
		scope.$watch(attrs.spec, function(value) {
			format = value;
			updateTime();
		});
		
		element.on('$destroy', function(){
			$interval.cancel(timeoutId);
		});
		
		timeoutId = $interval(function(){
			updateTime();
		}, 1000);
		
	}
	
	return {
		restrict: 'E',
		link: link
	}
	
}]);

highlineDirectives.directive('myDialog', function(){
	
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			close: '&onClose',
			up: '&onUp'
				
		},
		templateUrl: '/highline/resources/js/templates/directive-template-my-dialog.html'
	};
	
});

highlineDirectives.directive('myDraggable', ['$document', function($document) {
	
	return {
		restrict: 'E',
		link: function(scope, element, attr) {
			var startX = 0, startY = 0, x = 0, y = 0;
			
			element.css({
				position: 'relative',
				border: '2px solid #990000',
				backgroundColor: '#fff',
				cursor: 'pointer'
			});
			
			element.on('mousedown', function(event) {
				event.preventDefault();
				startX = event.pageX - x;
				startY = event.pageY - y;
				$document.on('mousemove', mousemove);
				$document.on('mouseup', mouseup);
			});
			
			function mousemove(event) {
				y = event.pageY - startY;
				x = event.pageX - startX;
				element.css({
					top: y + 'px',
					left: x + 'px'
				});
			}
			
			function mouseup() {
				$document.off('mousemove', mousemove);
				$document.off('mouseup', mouseup);
			}
		}
	
	};
	
}]);

highlineDirectives.directive('myTabsDemo', ['$log', function($log) {
	
	return {
		restrict: 'E',
		transclude: true,
		scope: {},
		controller: ['$scope', function($scope) {
			var panes = $scope.panes = [];
			
			$scope.select = function(pane) {
				angular.forEach(panes, function(pane) {
					pane.selected = false;
				});
				pane.selected = true;
			};
			
			this.addPane = function(pane) {
				if (panes.length === 0) {
					$scope.select(pane);
				}
				panes.push(pane);
			};
			
		}],
		templateUrl: '/highline/resources/js/templates/directive-template-my-tabs.html'
	};
	
}]);

highlineDirectives.directive('myPaneDemo', ['$log', function($log) {
	
	return {
		require: '^^myTabsDemo',
		restrict: 'E',
		transclude: true,
		scope: {
			title: '@'
		},
		link: function(scope, element, attrs, tabsCtrl) {
			tabsCtrl.addPane(scope);
		},
		templateUrl: '/highline/resources/js/templates/directive-template-my-pane.html'
	};
	
}]);