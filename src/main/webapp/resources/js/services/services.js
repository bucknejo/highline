'use strict';
 
var highlineServices = angular.module('highlineServices', ['ngResource']);

highlineServices.factory('Address', ['$resource', function($resource){
	return $resource('/highline/service/address/:id/:location_id', {}, {
		query: {method: 'GET', params: {}, isArray: true},
		update: {method: 'PUT'},
		retrieve: {method: 'GET', params: {
			location_id: '@location_id'
		}, isArray: true}
	});
}]);

highlineServices.factory('Friend', ['$resource', function($resource){
	return $resource('/highline/service/friend/:id', {}, {
		query: {method: 'GET', params: {}, isArray: true}
	});
}]);

highlineServices.factory('Gruppe', ['$resource', function($resource){
	return $resource('/highline/service/gruppe/:id', {id: '@id'}, {
		query: {method: 'GET', params: {}, isArray: true},
		update: {method: 'PUT'}
	});
}]);

highlineServices.factory('GruppeMember', ['$resource', function($resource){
	return $resource('/highline/service/gruppemember/:id/:gruppe_id/:user_id', {id: '@id'}, {
		query: {method: 'GET', params: {}, isArray: true},
		retrieve: {method: 'GET', params: {
			gruppe_id:'@gruppe_id',
			user_id:'@user_id'		
		}},
		update: {method: 'PUT'},
		remove: {method: 'DELETE', params: {
			gruppe_id:'@gruppe_id',
			user_id:'@user_id'						
		}}
		
	});
}]);

highlineServices.factory('Location', ['$resource', function($resource){
	return $resource('/highline/service/location/:id/:state', {id: '@id'}, {
		query: {method: 'GET', params: {}, isArray: true},
		retrieve: {method: 'GET', params: {
			state: '@state'
		}, isArray: true}
	});
}]);

highlineServices.factory('Member', ['$resource', function($resource){
	return $resource('/highline/service/member/:id', {}, {
		query: {method: 'GET', params: {}, isArray: true}
	});
}]);

highlineServices.factory('Photo', ['$resource', function($resource){
	return $resource('/highline/service/photo/get', {}, {
		query: {method: 'GET', params: {}, isArray: true}
	});
}]);

highlineServices.factory('Ride', ['$resource', function($resource){
	return $resource('/highline/service/ride/:id/:user_id', {id: '@id'}, {
		query: {method: 'GET', params: {}, isArray: true},
		retrieve: {method: 'GET', params: {
			user_id: '@user_id'
		}, isArray: true},
		update: {method: 'PUT'},		
	});
}]);

highlineServices.factory('User', ['$resource', function($resource){
	return $resource('/highline/service/user/:id', {id: '@id'}, {
		query: {method: 'GET', params: {}, isArray: true},
		update: {method: 'PUT'}
	});
}]);


highlineServices.factory('Sandbox', ['$resource', function($resource){
	console.log('in Sandbox factory');
	return $resource('/highline/service/ride/get', {}, {
		query: {method: 'GET', params: {}, isArray: true}
	});
}]);