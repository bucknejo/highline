
'use strict';

console.log('in application...');

if (typeof angular == undefined) {
	console.log('cannot find angular');
}

var highline = angular.module('highline', [
    'ngRoute',
    'ngMessages',
    'highlineControllers',
    'highlineFilters',
    'highlineServices',
    'highlineDirectives'
]);

highline.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
		$routeProvider.
			when('/', {
				templateUrl: '/highline/resources/partials/home.html',
				controller: 'HomeController'				
			}).		
			when('/dashboard', {
				templateUrl: '/highline/resources/partials/dashboard.html',
				controller: 'DashboardController'				
			}).		
			when('/friend/:id', {
				templateUrl: '/highline/resources/partials/friend.html',
				controller: 'FriendController'				
			}).		
			when('/gruppe/:id', {
				templateUrl: '/highline/resources/partials/gruppe.html',
				controller: 'GruppeController'				
			}).		
			when('/login', {
				templateUrl: '/highline/resources/partials/login.html',
				controller: 'LoginController'				
			}).		
			when('/photos', {
				templateUrl: '/highline/resources/partials/photos-list.html',
				controller: 'PhotoController'				
			}).				
			when('/register', {
				templateUrl: '/highline/resources/partials/register.html',
				controller: 'RegisterController'				
			}).		
			when('/rides/:id', {
				templateUrl: '/highline/resources/partials/ride.html',
				controller: 'RideController'
			}).
			when('/rides/detail/:id', {
				templateUrl: '/highline/resources/partials/ride-detail.html',
				controller: 'RideDetailController'
			}).
			when('/users', {
				templateUrl: '/highline/resources/partials/user-list.html',
				controller: 'UserController'				
			}).
			when('/users/detail/:id', {
				templateUrl: '/highline/resources/partials/user-detail.html',
				controller: 'UserDetailController'				
			}).
			
			// sandbox
			when('/sandbox', {
				templateUrl: '/highline/resources/partials/sandbox.html',
				controller: 'SandboxController'				
			}).									
			
			// demo start
			when('/accordion', {
				templateUrl: '/highline/resources/partials/demo/accordion.html',
				controller: 'AccordionController'				
			}).			
			when('/alert', {
				templateUrl: '/highline/resources/partials/demo/alert.html',
				controller: 'AlertController'				
			}).			
			when('/button', {
				templateUrl: '/highline/resources/partials/demo/button.html',
				controller: 'ButtonController'				
			}).			
			when('/carousel', {
				templateUrl: '/highline/resources/partials/demo/carousel.html',
				controller: 'CarouselController'				
			}).			
			when('/collapse', {
				templateUrl: '/highline/resources/partials/demo/collapse.html',
				controller: 'CollapseController'				
			}).			
			when('/dateparser', {
				templateUrl: '/highline/resources/partials/demo/dateparser.html',
				controller: 'DateparserController'				
			}).			
			when('/datepicker', {
				templateUrl: '/highline/resources/partials/demo/datepicker.html',
				controller: 'DatepickerController'				
			}).			
			when('/dropdown', {
				templateUrl: '/highline/resources/partials/demo/dropdown.html',
				controller: 'DropdownController'				
			}).			
			when('/modal', {
				templateUrl: '/highline/resources/partials/demo/modal.html',
				controller: 'ModalController'				
			}).			
			when('/tabs', {
				templateUrl: '/highline/resources/partials/demo/tabs.html',
				controller: 'TabsController'				
			}).			
			when('/timepicker', {
				templateUrl: '/highline/resources/partials/demo/timepicker.html',
				controller: 'TimepickerController'				
			}).			
			
			// demo end
			
			otherwise({
				redirectTo: '/'
			});
		
		/*
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: true
		});
		*/
	}
]);