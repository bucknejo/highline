<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!doctype html>
<html lang="en" ng-app="highline">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<head>	    
		<link rel="stylesheet" href="<c:url value="/resources/css/site.css" />" />    		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-cookies.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-resource.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-route.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.min.js"></script>	    
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>				
	    <script src="<c:url value="/resources/js/app.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/controllers.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/services/services.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/filters/filters.js" />" type="text/javascript"></script>			    
	</head>
	<body>	
		<div ng-include="'/highline/resources/partials/header.html'"></div>		
						
		<div ng-include="'/highline/resources/partials/footer.html'"></div>				
	</body>
</html>



