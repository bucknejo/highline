<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!doctype html>
<html lang="en" ng-app="highline">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<head>	    

		<!-- angular cdn 
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-cookies.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-resource.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-route.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-touch.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-messages.min.js"></script>
		-->

		<!-- angular local -->
		<script src="<c:url value="/resources/js/angular-1.5.3/angular.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-animate.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-cookies.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-resource.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-route.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-sanitize.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-touch.min.js" />" type="text/javascript"></script>
		<script src="<c:url value="/resources/js/angular-1.5.3/angular-messages.min.js" />" type="text/javascript"></script>
		
		<!-- bootstrap ui -->
	    <script src="<c:url value="/resources/js/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js" />" type="text/javascript"></script>
		
		<!-- application -->	 
	    <script src="<c:url value="/resources/js/app.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/controllers.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/services/services.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/filters/filters.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/directives/directives.js" />" type="text/javascript"></script>
	    
	    <script src="<c:url value="/resources/js/controllers/dashboard.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/friend.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/gruppe.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/header.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/home.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/login.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/member.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/photo.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/register.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/ride-detail.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/ride.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/user-detail.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/user.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/demo.js" />" type="text/javascript"></script>
	    <script src="<c:url value="/resources/js/controllers/sandbox.js" />" type="text/javascript"></script>
	    
		<!-- bootstrap cdn css 
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		-->	
		
		<!-- bootstrap local -->
		<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-3.3.6-dist/css/bootstrap.min.css" />">
						
		<link rel="stylesheet" href="<c:url value="/resources/css/site.css" />" />    		
		    			    
	</head>
	<body>	
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12" ng-include="'/highline/resources/partials/header.html'" ng-controller="HeaderController"></div>
			</div>
			<div class="row">		
				<div class="col-md-3" ng-include="'/highline/resources/partials/alley-left.html'"></div>				
				<div class="col-md-6" ng-view></div>
				<div class="col-md-3" ng-include="'/highline/resources/partials/alley-right.html'"></div>				
			</div>
			<div class="row">						
				<div class="col-md-12"  ng-include="'/highline/resources/partials/footer.html'"></div>
			</div>							
		</div>	
	</body>
</html>