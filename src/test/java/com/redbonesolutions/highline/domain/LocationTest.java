package com.redbonesolutions.highline.domain;


import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.redbonesolutions.highline.service.LocationService;
import com.redbonesolutions.highline.utility.HighlineUtility;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:servlet-context.xml", "classpath*:datasource.xml"})
public class LocationTest {

	@Autowired
	LocationService locationService;
	
	@Test
	public void CreateTest() {
		
		Location l = new Location();
		l.setDate_created(HighlineUtility.getCurrentDate());
		l.setLast_updated(HighlineUtility.getLastModified());
		l.setActive(1);
		l.setName("Tourne State Park");
		l.setDescription("Tourne State Park");
		l.setUser_id(0);
		l.setSteward(0);
		l.setAddress_id(0);
		l.setStatus("OPEN");
		locationService.save(l);
		long id = l.getId();
		l = null;
		Location ll = locationService.findOne(id);
		Assert.assertEquals("Tourne State Park", ll.getName());
		ll = null;
		
	}
	
	public LocationService getLocationService() {
		return locationService;
	}
	
	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}
	
}
